#include "stdafx.h"
#include "Book.h"

//Test Niklas 100 �L!!!!!
using namespace std;

Book::Book(string bookName, Author authorName, double bookPrice) {
	name = bookName;
	author = authorName;
	price = bookPrice;
}
Book::Book(string bookName, Author authorName, double bookPrice, int thisQty) {
	name = bookName;
	author = authorName;
	price = bookPrice;
	qty = thisQty;
}
string Book::getName() {
	return name;
}
Author Book::getAuthor() {
	return author;
}
double Book::getPrice() {
	return price;
}
void Book::setPrice(double bookPrice) {
	bookPrice = price;
}
int Book::getQty() {
	return qty;
}
void Book::setQty(int thisQty) {
	thisQty = qty;
}
string Book::toString() {
	stringstream ss;
	ss << "Book[name=" << name << "," << author.toString()
		<< ",price=" << price << ",qty=" << qty << "]";
	return ss.str();
}
