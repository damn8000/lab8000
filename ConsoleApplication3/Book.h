#include "stdafx.h"
#include <string>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include "Author.h"
//HEJ NIKLAS OCH POST-L
//HEJ DANIEL
using namespace std;

class Book {
private:
	string name;
	Author author;
	double price;
	int qty = 0;
public:
	Book(string bookName, Author AuthorName, double bookPrice);

	Book(string bookName, Author AuthorName, double bookPrice, int thisQty);

	string getName();

	Author getAuthor();

	double getPrice();
	
	void setPrice(double bookPrice);

	int getQty();

	void setQty(int thisQty);

	string toString();
};